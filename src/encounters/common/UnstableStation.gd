extends Encounter

func _ready() -> void:
	text("This friendly civilian space station is unstable! It's spining out of control. People inside are scared -- and with a good reason!")
	yield(GameEvents, "encounterTicked")
	text('Everyone in the station: "Ahhhhhhhhhhh!"')
	yield(GameEvents, "encounterTicked")
	text("You don't think twice and use your tractor beam to stabilize the station.")
	sound("bad_surprise")
	changeEnergy(-5)
	yield(GameEvents, "encounterTicked")

	text('The station people are still dizzy, but also relieved: "Thank you so much! We have some people here so impressed they are offering to join your crew."')
	yield(GameEvents, "encounterTicked")

	offerChoice("Who's comming along?",
				"A stereotypical programmer",
				"A stereotypical exotic dancer",
				"No one. This is ship is for professionals!")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text('He eats a lot but, oh, he is reasonable!')
			changeDReason(+0.4)
			changeDLifeSupport(-0.35)
		1:
			text("These dancing acts makes life more beareable, but it's hard to think with this beauty around!")
			changeDReason(-0.35)
			changeDLifeSupport(+0.35)
		2:
			text('Right, the current crew already causes enough trouble.')

	yield(GameEvents, "encounterTicked")
	text('Time to say goodbye!')
	yield(GameEvents, "encounterTicked")

	done()
