extends Encounter


func _ready() -> void:
	text("You decide to pay a visit to the tellurian ship.")
	yield(GameEvents, "encounterTicked")
	text("To your surprise, the captain of this human crew is an alien. He introduces himself, struggling with the language:")
	yield(GameEvents, "encounterTicked")
	text('"Hail, frends! Name\'s Blogh. Nicy to see sum customers hier!"')
	yield(GameEvents, "encounterTicked")
	text('"I own pet shop!"')
	yield(GameEvents, "encounterTicked")
	text('"You mybe wanna buy tribbles?"')
	yield(GameEvents, "encounterTicked")
	text('Tribbles? This name rings a bell.')
	yield(GameEvents, "encounterTicked")

	offerChoice("Buy tribbles?",
				"Sure, we need pets!",
				"No, I am allergic to animal fur.")
	yield(GameEvents, "encounterTicked")

	if GameState.choice == 0:
			text('You buy the tribbles and bring them to the ship. They are the cuttest animals, but reproduce faster than rabbits!')
			changeDLifeSupport(-0.5)
			yield(GameEvents, "encounterTicked")

	text('Back to the bridge, you order to crew o resume the trip.')
	yield(GameEvents, "encounterTicked")

	done()
