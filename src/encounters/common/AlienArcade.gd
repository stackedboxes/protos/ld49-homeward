extends "res://encounters/base/AlienShipEncounter.gd"

func _ready() -> void:
	text("You decide to check the station. Nobody is around, all is silent.")
	yield(GameEvents, "encounterTicked")
	text("You wander a little more and start listening some weird sounds and music. You walk towards the sounds.")
	$ArcadeAudioPlayer.volume_db = -40
	$ArcadeAudioPlayer.play()
	yield(GameEvents, "encounterTicked")
	$Aliens.visible = true
	text("What a surprise! This is an alien arcade! Lot's of games to play!")
	$ArcadeAudioPlayer.volume_db = 0
	yield(GameEvents, "encounterTicked")

	offerChoice("Play some games?",
				"Sure!",
				"No, games are for children.")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text("You and the crew have a lot of fun! It's was great to forget the problems the ship is having for a moment.")
			changeReason(-2.2)
		1:
			text("You get some disappointed looks from the crew.")

	yield(GameEvents, "encounterTicked")

	done()
