extends "res://encounters/base/LandEncounter.gd"


func _ready() -> void:
	text("You scan the planet's surface. While some areas seem to thrive with life, vast portions of it are used as a huge landfill.")
	yield(GameEvents, "encounterTicked")
	text('Who would do that to a planet?!')
	yield(GameEvents, "encounterTicked")
	text('Hmm, but since nobody is looking around...')
	yield(GameEvents, "encounterTicked")

	offerChoice("Dump some batteries onto the planet?",
				"Yes, dump some.",
				"Yes, dump a lot.",
				"Why would I dump charged batteries?")
	yield(GameEvents, "encounterTicked")

	match(GameState.choice):
		0:
			text('Your batteries are but a speck in the mountains of garbage.')
			$Garbage/PGSmall.visible = true
			changeEnergy(-2.0)
		1:
			text('You make a new mound on top of the garbage highlands.')
			$Garbage/PGLarge.visible = true
			changeEnergy(-4.0)
		2:
			text('You keep the peace of mind.')

	yield(GameEvents, "encounterTicked")
	text('"Ahead! We have a long way to go!"')
	yield(GameEvents, "encounterTicked")

	done()
