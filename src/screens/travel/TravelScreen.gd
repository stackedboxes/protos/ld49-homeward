extends Node2D

# Probability of an encounter
var encounterProb := 0.0

# Engine volumes
const ENGINE_HIGH_VOLUME_DB = -15.0
const ENGINE_LOW_VOLUME_DB = -50.0

# Time to decelerate and stop before an encounter
const STOPPING_TIME = 4.0

const COMMON_ENCOUNTERS_PATH = "res://encounters/common"


func _ready() -> void:
	GameState.initialize()
	SceneStack.setInitialScene(self)
	$Graph.addPoint()
	GameEvents.connect("gameOvered", self, "_onGameOvered")
	GameEvents.connect("reachedEarth", self, "_onReachedEarth")

	var introPackedScene = load("res://encounters/intro/Intro.tscn")
	SceneStack.push("res://screens/encounter/EncounterScreen.tscn", introPackedScene.instance())


func _onTimerTickerTimeout() -> void:
	if GameState.isEndingTrip:
		return

	GameState.passTime(1.2)
	$Graph.addPoint()

	encounterProb += 0.015
	if RNG.bernoulli(encounterProb):
		triggerEncounter()
		encounterProb = 0.0


func _onGameOvered(cause: int) -> void:
	$TimeTicker.stop()
	$GameOverLabel.visible = true
	yield(get_tree().create_timer(5.0), "timeout")
	SceneStack.replaceTop("res://screens/game_over/GameOverScreen.tscn", cause)


func _onReachedEarth() -> void:
	$TimeTicker.stop()
	$SuccessLabel.visible = true
	$EncounterLabel.text = "This planet looks familiar... home!"
	$EncounterLabel.visible = true
	var earthPackedScene = load("res://props/Earth.tscn")
	spawnEncounterObject(earthPackedScene.instance())
	$Tween.start()
	yield(get_tree().create_timer(STOPPING_TIME), "timeout")
	$EngineAudioPlayer.stop()
	$PlayerShip.turnEnginesOff()

	yield(get_tree().create_timer(2.5), "timeout")


	SceneStack.replaceTop("res://screens/success/SuccessScreen.tscn")


func triggerEncounter() -> void:
	$TimeTicker.stop()
	var encounter = randomCommonEncounter()
	spawnEncounterObject(encounter.travelScreenScene.instance())
	$EncounterLabel.text = encounter.travelScreenText
	$EncounterLabel.visible = true
	$Tween.interpolate_property($EngineAudioPlayer, "volume_db", null,
		ENGINE_LOW_VOLUME_DB, STOPPING_TIME, $Tween.TRANS_QUAD, $Tween.EASE_IN_OUT)
	$Tween.interpolate_property($Starfield, "speed", null,
		0.0, STOPPING_TIME, $Tween.TRANS_QUAD, $Tween.EASE_IN_OUT)
	$Tween.start()

	yield(get_tree().create_timer(STOPPING_TIME), "timeout")

	$EngineAudioPlayer.stop()
	$PlayerShip.turnEnginesOff()

	yield(get_tree().create_timer(2.5), "timeout")

	$EncounterLabel.visible = false
	$EncounterThing.remove_child($EncounterThing.get_child(0))
	SceneStack.push("res://screens/encounter/EncounterScreen.tscn", encounter)


func spawnEncounterObject(node: Node) -> void:
	$EncounterThing.position.x = 1900
	$EncounterThing.add_child(node)
	$Tween.interpolate_property($EncounterThing, "position", Vector2(1900, 235),
		Vector2(900, 235), STOPPING_TIME,  $Tween.TRANS_QUAD, $Tween.EASE_IN_OUT)


func onDigOut(_arg) -> void:
	$Tween.interpolate_property($EngineAudioPlayer, "volume_db", null,
		ENGINE_HIGH_VOLUME_DB, 0.5, $Tween.TRANS_QUAD, $Tween.EASE_IN_OUT)
	$Tween.start()
	$EngineAudioPlayer.play()
	$PlayerShip.turnEnginesOn()
	$Starfield.speed = 1.0
	$TimeTicker.start()



var commonEncounters = [ ]

func randomCommonEncounter() -> Encounter:
	if len(commonEncounters) == 0:
		var dir = Directory.new()
		dir.open(COMMON_ENCOUNTERS_PATH)
		dir.list_dir_begin(true, true)

		while true:
			var file = dir.get_next()
			if file == "":
				break
			if !file.ends_with(".tscn"):
				continue

			var encounterPath := str(COMMON_ENCOUNTERS_PATH + "/" + file)
			commonEncounters.push_back(encounterPath)

		dir.list_dir_end()

	var name = RNG.drawOne(commonEncounters)
	var index = commonEncounters.find(name)
	if index != -1:
		commonEncounters.remove(index)
	var packedScene := load(name)
	return packedScene.instance() as Encounter
