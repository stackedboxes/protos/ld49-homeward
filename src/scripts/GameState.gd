extends Node

# The unstable variables and their rates of change.
var energy: float = -171.0
var dEnergy: float = -171.0
var lifeSupport: float = -171.0
var dLifeSupport: float = -171.0
var reason: float = -171.0
var dReason: float = -171.0

# Number of days of travel. Starts at 0.0. At 100.00 we reach the Earth. Passes
# only when travelling.
var time := -171.0

# When true, we are ending the trip -- either successfully or not.
var isEndingTrip = true

# Last option selected by the player.
var choice: int = -1


func initialize() -> void:
	energy = 50.0
	dEnergy = 0.0
	lifeSupport = 50.0
	dLifeSupport = 0.0
	reason = 50.0
	dReason = 0.0
	time = 0.0
	isEndingTrip = false


func passTime(dt: float) -> void:
	if isEndingTrip:
		return

	time += dt
	if time >= 100.0:
		isEndingTrip = true
		GameEvents.emit_signal("reachedEarth")
		return

	energy += dEnergy * dt
	lifeSupport += dLifeSupport * dt
	reason += dReason * dt

	var gameOverCause := -1
	if energy <= 0.0:
		gameOverCause = GameEvents.GameOverCause.ENERGY_LOW
	elif energy >= 100.0:
		gameOverCause = GameEvents.GameOverCause.ENERGY_HIGH
	elif lifeSupport <= 0.0:
		gameOverCause = GameEvents.GameOverCause.LIFE_SUPPORT_LOW
	elif lifeSupport >= 100.0:
		gameOverCause = GameEvents.GameOverCause.LIFE_SUPPORT_HIGH
	elif reason <= 0.0:
		gameOverCause = GameEvents.GameOverCause.REASON_LOW
	elif reason >= 100.0:
		gameOverCause = GameEvents.GameOverCause.REASON_HIGH

	if gameOverCause != -1:
		isEndingTrip = true
		GameEvents.emit_signal("gameOvered", gameOverCause)

	energy = clamp(energy, 0, 100)
	lifeSupport = clamp(lifeSupport, 0, 100)
	reason = clamp(reason, 0, 100)
