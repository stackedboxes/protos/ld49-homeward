extends Node2D

# In pixels per second
const MAX_SPEED := 100

# Relative, between 0 and 1.
export var speed := 0.0


func _process(delta: float) -> void:
	var s := MAX_SPEED * delta * speed
	$Stars1.rect_position.x -= s
	if $Stars1.rect_position.x < -1280:
		$Stars1.rect_position.x += 1280

	$Stars2.rect_position.x -= s * 0.5
	if $Stars2.rect_position.x < -1280:
		$Stars2.rect_position.x += 1280

